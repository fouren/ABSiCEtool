# ABSiCE Tool

The Agent-Based Simulations of the Circular Economy (ABSiCE) tool is 
an agent-based model built with the MESA python library. The tool enables 
studying various circular economy strategies by simulating actors decisions.
The model draws on the industrial symbiosis litterature.

## Features

* Network configuration
* Product growth and waste generation
* Agents end-of-life decision rule

## Requirements

* MESA
* networkx

